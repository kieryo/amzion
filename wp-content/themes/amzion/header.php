<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>	
	</head>
	<body <?php body_class(); ?>>
		
		<div class="main-content mx-auto">
			<!-- site-header -->
			<header class="site-header">
				<h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>

				<nav class="navbar navbar-expand-lg">
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
						<?php 
							$args = array(
								'theme_location' => 'primary',
								'menu_class'     => 'navbar-nav',
								'container'       => false
							);
						?>
						<?php wp_nav_menu( $args ); ?>
				  </div>
				</nav>

			</header>
			<!-- /site-header -->
