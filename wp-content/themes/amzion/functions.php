<?php

function amzion_resources() {

	// CSS
	wp_enqueue_style('bootstrap4',  get_template_directory_uri() . '/assets/libraries/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap4reset',  get_template_directory_uri() . '/assets/libraries/bootstrap/css/bootstrap-reboot.min.css');
	wp_enqueue_style('style', get_stylesheet_uri());

	// SCRIPTS
	wp_enqueue_script('jquery3.3.1',  get_template_directory_uri() . '/assets/libraries/js/jquery-3.3.1.min.js');
	wp_enqueue_script('bootstrapJs',  get_template_directory_uri() . '/assets/libraries/bootstrap/js/bootstrap.min.js');

}

add_action('wp_enqueue_scripts','amzion_resources');



// Navigation Menus
register_nav_menus(array(
	'primary'               => __( 'Primary Menu'),
	'footer'                => __( 'Footer Menu'),
	'footer_social_media'   => __( 'Footer Social Media'),
	'footer_left'           => __( 'Footer Left')
));

// ad class to li
function primary_menu_classes($classes, $item, $args) {
  if($args->theme_location == 'primary') {
    $classes[] = 'nav-item';
  }
  return $classes;
}
add_filter('nav_menu_css_class', 'primary_menu_classes', 1, 3);


// add class to anchor
function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

// add post thumbnail
add_theme_support( 'post-thumbnails' ); 

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );