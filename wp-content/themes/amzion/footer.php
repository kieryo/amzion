	
	</div> <!-- /main-content -->

	<footer class="site-footer">
		<div class="amzion-container">
			<div class="row">
				<div class="col-6 left-content">
					<nav class="socialmedia-links amzion-nav">
						<?php 
							$args = array(
								'theme_location' => 'footer_social_media',
								'container'      => false
							);
						?>
						<?php wp_nav_menu( $args ); ?>
					</nav>
					<nav class="tnc-links amzion-nav">
						<?php 
							$args = array(
								'theme_location' => 'footer_left',
								'container'      => false
							);
						?>
						<?php wp_nav_menu( $args ); ?>
					</nav>

				</div>
				<div class="col-6 right-content">
					<nav class="footer-nav amzion-nav">
						<?php 
							$args = array(
								'theme_location' => 'footer',
								'container'      => false
							);
						?>
						<?php wp_nav_menu( $args ); ?>
					</nav>

					<p class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> All Rights Reserved.</p>	
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	</body>
</html>