<?php get_header(); ?>

<!-- Banner Image -->
<section id="carousel" class="carousel">
	<div id="amzionCarousel" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">

			<?php 
				$field_value = simple_fields_fieldgroup("banner"); 
				if($field_value) {
					foreach($field_value as $key => $val) {
						$img = $val['banner_image'];
						if($key == 0) {
							$class = 'active';
						} else {
							$class = '';
						}
			?>
		    <div class="carousel-item <?php print $class; ?>">
		      <?php print wp_get_attachment_image($img,'full'); ?>
				  <div class="carousel-caption">
				    <h3><?php print $val['banner_title']; ?></h3>
				    <p><?php print $val['banner_text']; ?></p>
				  </div>
		    </div>
	    <?php  } ?>
	    <?php } //if fieldvalue ?>

	  </div>
	  <a class="carousel-control-prev" href="#amzionCarousel" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#amzionCarousel" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
</section><!-- carousel -->

<!-- News And Updates Section -->
<section id="newsUpdates" class="news-updates section-wrapper">
	<div class="section-header">
		<h2 class="text-grey">News and Updates</h2>
		<a href="#" class="amzion-button">View All</a>
	</div>
	<div class="section-content">
		<div class="row">
			<?php 
				// get posts for news and updates 
				$args = array (
					'category_name'  => 'updates,news',
					'posts_per_page' => 4
				);

				// The Query
				$the_query = new WP_Query( $args  );
			?>
			<?php
			// The Loop
			if ( $the_query->have_posts()) :
				while ( $the_query->have_posts()) :
					$the_query->the_post(); 
			    //get category
			    $categories = wp_get_post_categories( get_the_ID() );
			    $cat = get_category( $categories[0] );
			    $category = $cat->slug;
			?>
				<div class="col-md-3 news-updates-wrapper <?php echo $category; ?>">
					<div class="thumbnail-wrapper">
						<?php 
						if(has_post_thumbnail()): 
						 	echo get_the_post_thumbnail(); 
						else: ?>
							<img src="http://localhost/sideline/amzion/wp-content/uploads/2018/08/news.jpg" alt="">
						<?php endif; ?>
					</div>
					<div class="content">
						<span class="title"><?php the_title(); ?></span>
						<span class="date"><?php echo get_the_date(); ?></span>
						<p><?php the_excerpt(); ?></p>
						<a href="<?php the_permalink(); ?>">Know More</a>
					</div>
				</div>
			<?php
				endwhile;
				/* Restore original Post Data */
				wp_reset_postdata();
			else :
				// no posts found
			endif;

			?>
		</div>
	</div>
</section> <!-- /news and updates -->


<?php get_footer(); ?>
