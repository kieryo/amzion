<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'amzion_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A*gQ:FDKxnu>L=KNjIwd3c5{YUwD_s(U+7O~i_g ts8Dk>$D-B&>KiY,!k&jK$;9');
define('SECURE_AUTH_KEY',  '4>tSmu*3jqPS%t+F4)d;1eIab[>Hk<:bA{T,x)5?34;KF@>CfD:2.Ty&<0Jo.ITx');
define('LOGGED_IN_KEY',    '%KGvI8m)`mL;6}J9;09A]B!L)u*wDI69A{+oYuqDGBz^a:nA`ojBfl5Pz}]!=W#G');
define('NONCE_KEY',        '0f#FH9DD5a`jhiOO%HU+WCz2[DL:xh=n#Vq?TnGn1)@sE.zs/7gQ~DO<P8V#lj-c');
define('AUTH_SALT',        '=WoBv!w-Xd,svUjpwx7UIO[5$0/nfJ}x-WZBwxH6nz28ypC/]KUK%dEz0[v=>L#l');
define('SECURE_AUTH_SALT', '8@<&PSRQr|:#m?f|_J2k>oV$KAVy-lcytKmRLFeM*[=kU#7Hgdef:0fz7ea-X7>u');
define('LOGGED_IN_SALT',   't+{lcG.?) G=nDG3l4kUe8W_#kRZ+0<P9&qMad@:hr;KgqlW|y/kj{aJU24^|VSm');
define('NONCE_SALT',       'vd<^F&8vh2LUl$,o7pI.k%y]J4u-fTU{peTf$zkNnxj@I9_/x,>& @|JCUob<B(K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
